/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;

@Entity
@Table(name = "student")
@JacksonXmlRootElement(localName = "Person")
public class Student implements Serializable {

    @Id
    private Long id;

//    public Student(String name, String email, String contact, int age) {
//        this.name = name;
//        this.email = email;
//        this.contact = contact;
//        this.age = age;
//    }
    public Student() {
    }

    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", email=" + email + ", contact=" + contact + ", age=" + age + ", dob=" + dob + '}';
    }

    @JacksonXmlProperty(localName = "name")
    @JsonProperty("name")
    @Column(name = "name")
    private String name;

    @JacksonXmlProperty(localName = "contact")
    @JsonProperty("contact")
    @Column(name = "contact")
    private String contact;

    @JacksonXmlProperty(localName = "email")
    @JsonProperty("emial")
    @Column(name = "email")
    private String email;

    @JacksonXmlProperty(localName = "age")
    @JsonProperty("age")
    @Column(name = "age")
    private int age;

    @JacksonXmlProperty(localName = "dob")
    @JsonProperty("dob")
    @Column(name = "dob")
    private String dob;

    @XmlElement
    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @XmlElement
    public String getName() {
        return name;
    }

    @XmlElement
    public int getAge() {
        return age;
    }

    @XmlElement
    public String getEmail() {
        return email;
    }

    @XmlElement
    public String getContact() {
        return contact;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
