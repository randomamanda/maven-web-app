/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.service;

import Model.Student;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class StudentService {

    ResultSet rs = null;
    HikariDataSource ds;
    Connection con;

    public StudentService() throws SQLException, IOException {
        this.con = getConnection();
    }
   
    
    private Connection getConnection() throws SQLException, IOException {
        InputStream stream = getClass().getClassLoader().getResourceAsStream("db.properties");
        Properties prop = new Properties();
        prop.load(stream);        

        HikariConfig cfg = new HikariConfig(prop);
        ds = new HikariDataSource(cfg);
        con = ds.getConnection();
        return con;
    }

    public void persist(Student student) throws SQLException, IOException {
        String query = "INSERT INTO student (name,dob,email,contact, age) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement st = con.prepareStatement(query);
        st.setString(1, student.getName());
        st.setString(2, student.getDob());
        st.setString(3, student.getEmail());
        st.setString(4, student.getContact());
        st.setInt(5, student.getAge());
        st.executeUpdate();
        System.out.println("data enterd");
        st.close();
        closeConnection();

    }

    private void closeConnection() {
        try {

            if (rs != null) {
                rs.close();
            }

            if (con != null) {
                con.close();
            }

            ds.close();

        } catch (SQLException ex) {

//                Logger lgr = Logger.getLogger(HikariCPEx.class.getName());
//                lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

}
