/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.service.StudentService;
import Model.Student;
import Service.ValidateService;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.io.IOException;
import java.sql.SQLException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
//import jdk.internal.org.xml.sax.SAXException;
import org.json.XML;
import org.xml.sax.SAXException;

/**
 *
 * @author ACER
 */
@Path("/student")
public class StudentController {
    @Path("/serialize")
    @POST
    @Consumes("application/xml")
    @Produces("application/json")
    public Response parseToJson(String xml) throws IOException, SQLException, ClassNotFoundException {
        XmlMapper xmlMapper = new XmlMapper();
        Student student = xmlMapper.readValue(xml, Student.class);
        String json = XML.toJSONObject(xml).toString();        
        System.out.println(student);       
        StudentService serv=new StudentService();
        serv.persist(student);
        return Response.status(200).entity(json).build();
    }
     @Path("/validate")
    @POST
    @Consumes("application/xml")
    @Produces("application/xml")
    public Response getpersonDetails(String data) throws SAXException {
        ValidateService service=new ValidateService();
        
        if (service.isValid(data)) {
            return Response.status(200).entity("Details are valid").build();
        } else {
            return Response.status(400).entity("Invalid Details").build();
        }

    }

    
}
