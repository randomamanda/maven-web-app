/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.xml.sax.SAXException;

/**
 *
 * @author ACER
 */
public class ValidateService {
    
   public  boolean isValid(String xml) throws SAXException {
//        String xsd = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
//                + "<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" attributeFormDefault=\"unqualified\"\n"
//                + "           elementFormDefault=\"qualified\">\n"
//                + "    <xs:element name=\"Person\" type=\"rootType\">\n"
//                + "    </xs:element>\n"
//                + "\n"               
//                + "    <xs:complexType name=\"recordType\">\n"
//                + "        <xs:sequence>\n"
//                + "            <xs:element type=\"xs:string\" name=\"name\"/>\n"
//                + "            <xs:element type=\"xs:string\" name=\"contact\"/>\n"
//                + "            <xs:element type=\"xs:string\" name=\"email\"/>\n"
//                + "            <xs:element type=\"xs:positiveInteger\" name=\"age\"/>\n"
//                + "        </xs:sequence>\n"
//                + "    </xs:complexType>\n"
//                + "</xs:schema>";

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        try {
            //Schema schema = schemaFactory.newSchema(new File(getResource("schema.xsd")));
            //Schema schema = schemaFactory.newSchema(getClass().getClassLoader().getResourceAsStream("db.properties"));
            //Schema schema = schemaFactory.newSchema(getClass().getClassLoader().getResourceAsStream("schema.xsd"));
            Schema schema = schemaFactory.newSchema( new StreamSource(getClass().getClassLoader().getResourceAsStream("schema.xsd")));

            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new StringReader(xml)));
            return true;
        } catch (IOException e) {            
            return false;
        } catch (org.xml.sax.SAXException ex) {
           Logger.getLogger(ValidateService.class.getName()).log(Level.SEVERE, null, ex);
           // System.out.println("sax exc");
           return false;
       }

    }

    
}
